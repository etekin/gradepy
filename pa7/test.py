import os
import subprocess
from typing import List, Tuple
import re
import shutil
import logging as logger
import glob
import sys
import math


class PA7:
    def __init__(self, userID, commitID) -> None:
        self.userID = userID
        self.commitID = commitID

        if self.commitID is not None:
            self.checkout_commit()
        # Let's check if pa4 folder still exists
        self.not_grade = False
        if not (os.path.exists('pa7') and os.path.isdir('pa7')):
            self.not_grade = True
            logger.error(f"pa7 directory does not exist. Abort.")
            return

        os.chdir(f'pa7')

        # Get working directory
        self.cwd = os.getcwd()
        self.content_dir = os.path.join(self.cwd, '../gradepy/pa7/files')


        # Check excess files before copying test files
        self.penalty = self.excess_files()

        for file in os.listdir(self.content_dir):
            shutil.copy(os.path.join(self.content_dir, file), self.cwd)

        self.rubric = [
            self.check_dict_func,
            self.check_order_func,
            self.check_selftest_chck,
            self.check_formatting,
            self.check_makefile,
            self.check_readme,
            self.check_charity
        ]

    def try_open_file_with_encodings(self, file_path):
        common_encodings = ['utf-8', 'latin1', 'ascii', 'utf-16', 'utf-32']
        for encoding in common_encodings:
            try:
                with open(file_path, encoding=encoding) as file:
                    return file.read()
            except UnicodeDecodeError:
                logger.info(f"Failed to open the file with encoding: {encoding}")
            except FileNotFoundError:
                logger.info("File not found. Please check the file path.")
                break
            except Exception as e:
                logger.info(f"An error occurred: {e}")
                break
        else:
            logger.info("None of the encodings worked.")

    def is_binary_file(self, filepath, threshold=0.30):
        """
        Determines if a file is binary or text.
        It reads a portion of the file, and if the percentage of text-compatible
        characters is below the threshold, it is considered binary.
        """
        textchars = bytearray({7,8,9,10,12,13,27} | set(range(0x20, 0x100)) - {0x7f})
        
        with open(filepath, 'rb') as file:
            chunk = file.read(1024)
            if b'\x00' in chunk:  # Checking for null byte
                return True  # Contains null byte, likely a binary file

            # Count text-compatible characters
            nontext = sum(byte not in textchars for byte in chunk)

            # Calculate the percentage of non-text characters
            if nontext / (len(chunk)+1) > threshold:
                return True  # Higher percentage of non-text characters, likely binary

        return False

    def _run_command(self, command: str) -> Tuple[str, str, int]:
        process = subprocess.Popen(
            command,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            text=True,
            shell=True
        )
        stdout, stderr = process.communicate()
        logger.debug(
            f'COMMAND: {command}\n' + \
            f'STDOUT: {stdout.strip()}\n' + \
            f'STDERR: {stderr}\n' + \
            f'EXITCODE: {process.returncode}'
        )
        return stdout.strip(), stderr, process.returncode

    def checkout_commit(self):
        logger.debug(f'Checking out to {self.commitID}.')
        command = f'git checkout {self.commitID}'
        stdout, stderr, returncode = self._run_command(command)
        if returncode > 0:
            logger.error(f'Checkout failed!')
            return False
        return True

    def calculate_points(self, blocks):
        point_ranges = {
            (1, 20): -1,
            (21, 50): -2,
            (51, 100): -3,
            (101, 200): -4,
            (201, sys.maxsize): -5
        }

        for (start, end), points in point_ranges.items():
            if start <= blocks <= end:
                return points

        return 0

    def check_valgrind_summary(self, summary: str):
        num_blk_leaks = 0
        all_heap_free = re.search(r'All heap blocks were freed -- no leaks are possible', summary)
        if not all_heap_free:
            block_leaks = re.findall(
                r'(definitely|indirectly|possibly|still reachable|suppressed) lost: ([0-9]*) bytes in ([0-9]*) blocks',
                summary)
            for leak_type, bytes_leaked, num_blocks in block_leaks:
                num_blk_leaks += int(num_blocks)

        num_errs = 0
        num_errors = re.search(r'ERROR SUMMARY: ([0-9]*) errors from ([0-9]*) contexts', summary)
        if num_errors:
            num_errs = int(num_errors.group(1))

        return num_blk_leaks, num_errs

    def check_dict_func(self) -> int:
        # Check Dictionary functionality and memory leaks,
        # Returns pts taken
        pts = maxpts = 59
        logger.info(f"STARTING Dictionary Functionality and Memory Leak Check.")
        # test.0: Build

        command = 'g++ -std=c++17 -Wall -c -g ModelDictionaryTest.cpp Dictionary.cpp && g++ -std=c++17 -Wall -o ModelDictionaryTest ModelDictionaryTest.o Dictionary.o'
        stdout, stderr, returncode = self._run_command(command=command)
        build = True
        if returncode != 0:
            # FAILED
            pts -= 59
            logger.info('Could NOT compile.')
            build = False

        if build:
            abn_termination = False
            command = 'timeout 60 valgrind --leak-check=full -v ./ModelDictionaryTest -v'
            stdout, stderr, returncode = self._run_command(command=command)
            mem_blck_leaks, num_errors = \
                        self.check_valgrind_summary(stderr)
            # Timeout
            if returncode == 124:
                # FAILED
                abn_termination = True
                pts -= 49
                logger.info(f'Failed Dictionary Test: Timeout Exceeded, Charity Points Awarded.')

            # Abnormal termination
            elif returncode > 0:
                # FAILED
                abn_termination = True
                pts -= 49
                logger.info(f'Failed Dictionary Test: Abnormal termination EXIT CODE {returncode}, Charity Points Awarded')

            elif 'Receiving charity points because your program crashes' in stdout:
                # FAILED
                abn_termination = True
                pts -= 49
                logger.info(f'Failed Dictionary Test: Program crashes, Charity Points Awarded.')

            else:
                # PASSED
                # Parse Grading Script Result
                test_result = re.search(r'You will receive ([0-9]*) out of ([0-9]*) possible points on the Dictionary Functionality Test', stdout)
                full_func_pts = int(test_result.group(2))
                collected_pts = int(test_result.group(1))
                deduction = full_func_pts - collected_pts
                if deduction == 0:
                    logger.info('Passed Dictionary Test.')
                else:
                    pts -= deduction
                    test_result = re.search(r'You passed ([0-9]*) out of ([0-9]*) tests', stdout)
                    logger.info(f'Partial Pass Dictionary Test: Passed {test_result.group(1)} out of {test_result.group(2)} tests.')

            # Look at total memory leaks
            if abn_termination:
                pts -= 5
                logger.info(f'Failed Dictionary Memory Leak: Abnormal termination.')
            elif mem_blck_leaks == 0 and num_errors == 0:
                logger.info(f'Passed Memory Leak and Memory Error Tests.')
            elif mem_blck_leaks == 0 and num_errors > 0:
                pts -= 1
                logger.info(f'Partial Pass Dictionary Memory Leak: All memories are freed but there are still errors. #ofBlockLeaks: {mem_blck_leaks} #ofMemErors: {num_errors}')
            elif mem_blck_leaks < 20 and num_errors > 0:
                pts -= 2
                logger.info(f'Partial Pass Dictionary Memory Leak: The amount of block leaks is less than 20 + errors. #ofBlockLeaks: {mem_blck_leaks} #ofMemErors: {num_errors}')
            elif mem_blck_leaks < 200 and num_errors > 0:
                pts -= 3
                logger.info(f'Partial Pass Dictionary Memory Leak: The amount of block leaks is 20 < x < 200 + errors. #ofBlockLeaks: {mem_blck_leaks} #ofMemErors: {num_errors}')
            elif mem_blck_leaks >= 200 and num_errors > 0:
                pts -= 4
                logger.info(f'Partial Pass Dictionary Memory Leak: The amount of block leaks is greater than 200 + errors. #ofBlockLeaks: {mem_blck_leaks} #ofMemErors: {num_errors}')

        pts = max(0, min(math.ceil(pts), maxpts))
        logger.info(f'FINISHED Dictionary Functionality and Memory Leak Check. Collected {pts} out of {maxpts} points.')
        return pts

    def check_order_func(self) -> int:
        # Check Order functionality,
        # Returns pts taken
        pts = maxpts = 18
        logger.info(f"STARTING Order Functionality and Memory Check.")
        # test.0: Build
        build = True
        if os.path.exists('Order.cpp'):
            # Clean .o and binaries 
            self._run_command('rm -f *.o Order')

            # Compile
            command = 'g++ -std=c++17 -Wall -c -g Order.cpp Dictionary.cpp && g++ -std=c++17 -Wall -o Order Order.o Dictionary.o'
            stdout, stderr, returncode = self._run_command(command=command)
            if returncode != 0:
                # FAILED
                pts -= 18
                logger.info('Could NOT compile.')
                build = False
        else:
            # FAILED
            pts -= 18
            logger.info('Order.c does NOT exist.')
            build = False
            
        # test.1: Order functionality check
        if build:
            points_per_test = 15/5
            points_per_test_mem = 4/5
            for num in range(1,6):
                abn_termination = False
                # Create outfile file for each infile
                infile = f'infile{num}.txt'
                outfile = f'outfile{num}.txt'
                command = f'timeout 60 valgrind --leak-check=full -v ./Order {infile} {outfile}'
                stdout, stderr, returncode = self._run_command(command)
                # If outfile is successfully created
                if returncode == 0:
                    # test.2 Valgrind checks for the last run
                    mem_blck_leaks, num_errors = \
                        self.check_valgrind_summary(stderr)

                    correct_outfile = f'model-outfile{num}.txt'
                    command = f'diff -bBwu {outfile} {correct_outfile}'
                    stdout_diff, stderr_diff, returncode_diff = self._run_command(command)
                    # If diff is successful
                    if returncode_diff == 0:  
                        # No diff output passes func
                        if not (len(stderr_diff) == 0 and len(stdout_diff) == 0):
                            # FAILED Func 
                            pts -= points_per_test
                            logger.info(f'Failed Order Test {num}: Non-empty diff output.')
                        else:
                            logger.info(f'Passed Order Test {num}.')
                    else:
                        # FAILED Func
                        logger.info(f'Failed Order Test {num}: Did you print to stdout?')
                        pts -= points_per_test

                    if mem_blck_leaks == 0 and num_errors == 0:
                        logger.info(f'Passed Memory Leak and Memory Error Test {num}.')
                    else:
                        pts -= points_per_test_mem
                        logger.info(f'Partial Pass Memory Leak {num}: #ofBlockLeaks: {mem_blck_leaks} #ofMemErors: {num_errors}')
                    

                elif returncode > 0:
                    # FAILED
                    pts -= (points_per_test + points_per_test_mem)
                    logger.info(f'Failed Order  and Mem Test {num}: Abnormal termination EXIT CODE {returncode}')           

        pts = max(0, min(math.ceil(pts), maxpts))
        logger.info(f'FINISHED Order Functionality and Memory Check. Collected {pts} out of {maxpts} points.')
        return pts

    def check_selftest_chck(self) -> int:
        # Check student defined tests, does NOT do semantic test
        logger.info('STARTING DictionaryTest Check.')
        pts = maxpts = 5

        # Check whether ListTest.c exists
        if not os.path.exists('DictionaryTest.cpp'):
            #FAILED
            pts -= 5
            logger.info(f'Failed DictionaryTest Check: DictionaryTest.cpp does NOT exist.')
        else:
            # Check if ListTest.c is empty
            if os.path.getsize('DictionaryTest.cpp') < 50:
                pts -= 5
                logger.info(f'Failed DictionaryTest Check: DictionaryTest.cpp is less than 50 bytes (empty).')

            else:
                # Count added lines to ListTest
                command = 'diff -bBwu DictionaryClient.cpp DictionaryTest.cpp | grep ^+ | wc -l'
                stdout, stderr, returncode = self._run_command(command=command)
                if int(stdout.strip()) < 0:
                    #ListClient and ListTest are same
                    pts -= 4
                    logger.info(f'Failed DictionaryTest Check: DictionaryTest.cpp is only {int(stdout)} lines different from DictionaryClient.cpp')

                else:
                # ListTest compile
                    command = 'g++ -c -std=c++17 -Wall -g DictionaryTest.cpp Dictionary.cpp && g++ -o DictionaryTest DictionaryTest.o Dictionary.o'
                    stdout, stderr, returncode = self._run_command(command=command)
                    if returncode > 0:
                        #FAILED
                        pts -= 4
                        logger.info(f'Failed DictionaryTest Check: DictionaryTest did NOT compile.')
                    else:
                        #PASSED 
                        logger.info(f'Passed DictionaryTest Check.')

        logger.info(f'FINISHED DictionaryTest Check. Collected {pts} out of {maxpts} points.')
        return pts
    
    def check_formatting(self) -> int:
        # Header Format
        # /***
        # * Engin Tekin // First and Last Name
        # * etekin // UCSC UserID
        # * 2023 Fall CSE101 PA{ID} // Replace ID with assignment number
        # * ListClient.c // FileName
        # * Test client for List ADT // Description
        # ***/

        # We do NOT carry extensive testing here. Just check if file header block and userID
        logger.info('STARTING formatting check.')
        pts = maxpts = 4
        files_of_interest = ['Order.cpp', 'Dictionary.cpp']

        for file in files_of_interest:
            if os.path.exists(file):
                # with open(file, 'r') as f:
                #     content = f.read()
                content = self.try_open_file_with_encodings(file)
                    # Header block
                    # header_start = re.search(r'\/\*+', content)
                    # header_end = re.search(r'\*+\/', content)
                    # if not header_start or not header_end:
                    #     #FAILED
                    #     pts -= 2.5
                    #     logger.info(f'Failed {file} fmt test: Header block does NOT exist or not in the required format, please check ED post.')
                    #     continue

                    # User ID check
                userID = re.search(self.userID, content)
                if not userID:
                    pts -= 2
                    logger.info(f'Partial Pass {file} fmt test: UserID does NOT exists or wrong.')

                else:
                    # PASSED
                    logger.info(f'Passed {file} fmt test.')

            else:
                pts -= 2
                logger.info(f'Failed {file} fmt test: {file} does NOT exist.')

        logger.info(f'FINISHED Formatting Check.. Collected {pts} out of {maxpts} points.')
        return pts

    def check_makefile(self):
        logger.info('STARTING Makefile check.')
        pts = maxpts = 2

        # Check that Makefile exists
        if not os.path.exists('Makefile'):
            pts -= 2
            logger.info(f'Failed Makefile test: Makefile does NOT exist.')

        else:
            # Clean .o and binaries
            self._run_command('rm -f *.o Order')

            # Build Lex
            self._run_command('make')
            if os.path.exists('Order'):
                # PASSED
                logger.info(f'Passed Makefile test.')
            else:
                pts -= 2
                logger.info(f'Failed Makefile test: Order binary does NOT exist.')

        logger.info(f'FINISHED Makefile check. Collected {pts} out of {maxpts} points.')
        return pts

    def check_readme(self):
        logger.info('STARTING README check.')
        pts = maxpts = 2
        readme_path = glob.glob('README*')
        if len(readme_path) == 0:
            # FAILED
            pts -= 2
            logger.info(f'Failed README test: README.md does NOT exist.')
        else:
            # Check if README.md mentions each student submitted file
            # with open(readme_path[0], 'r') as f:
            #     content = f.read().lower()
            content = self.try_open_file_with_encodings(readme_path[0]).lower()
            student_submitted_files = ['Dictionary.cpp', 'Order.cpp']
            for file in student_submitted_files:
                if file.lower() not in content:
                    if pts > 1:
                        pts -= 1
                    logger.info(f'Partial Pass README test: {file} is NOT mentioned in {readme_path[0]}')

        pts = max(0, pts)
        logger.info(f'FINISHED README check. Collected {pts} out of {maxpts} points.')
        return pts

        pass

    def check_charity(self):
        student_submitted_files = ['Order.cpp', 'DictionaryTest.cpp', 'Dictionary.cpp', 'Dictionary.h', 'Makefile']
        listdir = os.listdir(self.cwd)
        for file in student_submitted_files:
            if file in listdir:
                logger.info(f'CHARITY POINTS AWARDED: {10} points.')
                return 10
        logger.info(f'NO CHARITY POINTS AWARDED: None of the required files submitted.')
        return 0

    def excess_files(self):
        # Deduct points if repo contains unnecassary files
        listdir = os.listdir(self.cwd) 
        penalty = 0
        for file in listdir:
            # Empty file/folder issue
            if os.path.exists(file):
                # Check if a dir, binary or txt file
                if os.path.isdir(file) or \
                    file.endswith('.txt') or \
                        self.is_binary_file(file):
                    logger.info(f'WARNING Do NOT push folders/binaries/textfile to repo. Will deduct 1 point: {file}')
                    penalty += 1

        return penalty

    def grade(self):
        # Grade according to rubric, return grade and breakdown
        collected_pts = 0
        for test in self.rubric:
            pts = test()
            collected_pts += pts
        
        return max(0, collected_pts - self.penalty)
