import argparse
import importlib
import logging
import datetime
import os

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Test or Grade Programming Assignment')
    parser.add_argument('-p', '--pa', type=int, default=1, help='assignment number')
    parser.add_argument('--grade', action='store_true', help='test or grade?')
    parser.add_argument('--user', type=str, help='userID of the student')
    parser.add_argument('--commit', type=str, help='commitID of the assignment', default=None)
    args = parser.parse_args()
    
    # set logget to DEBUG
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    # create file handler which logs even debug messages
    db = logging.FileHandler(f'PA{args.pa}_debug.txt', mode='w')
    db.setLevel(logging.DEBUG)

    # create file handler which logs even debug messages
    gr = logging.FileHandler(f'PA{args.pa}_grade.txt', mode='w')
    gr.setLevel(logging.INFO)

    # add handlers to logger
    logger.addHandler(db)
    logger.addHandler(gr)

    if args.grade:
        pa_module = importlib.import_module(f'pa{args.pa}.test')
        pa_cls = getattr(pa_module, f'PA{args.pa}')
        logger.info(f'USERID: {args.user}')
        logger.info(f'TIMESTAMP: {datetime.datetime.now()}')
        logger.info(f'COMMIT-ID: {args.commit}')
        logger.info(f'STARTING PA{args.pa} grading. Maxpts: 100')
        
        grader = pa_cls(args.user, args.commit)
        grade = 0
        if grader.not_grade is False:
            grade = grader.grade()
        logger.info(f'FINISHED PA{args.pa} grading. GRADE: {grade} out of 100.')